# Ecolo'Resto - Application Mobile

Ecolo'Resto est un projet réalisé par Nicolas KERVOERN, Pierre BOUDON et Nicolas GIDON, dans le cadre du projet annuel de 3ème année en Ingénierie des Applications Mobiles (années 2015-2016) à l'ESGI.

#### Ce repository contient l'application mobile du projet développée en Swift. 

Ce projet est décomposé en plusieurs repositories, chaque repository concernant une partie du projet : 

  - Les documents importants : 
    - Ecolo'RestoDocs_Repo
  - L'application Mobile (Swift) : 
    - Ecolo'RestoMobileApp_Repo
  - L'application Desktop (JAVA) : 
    - Ecolo'RestoDesktopApp_Repo
  - L'API (PHP/MySQL) : 
    - Ecolo'RestoAPI_Repo

### Version
1.0